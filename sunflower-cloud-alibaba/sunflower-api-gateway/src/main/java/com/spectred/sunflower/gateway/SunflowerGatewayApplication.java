package com.spectred.sunflower.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class SunflowerGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(SunflowerGatewayApplication.class, args);
    }
}
