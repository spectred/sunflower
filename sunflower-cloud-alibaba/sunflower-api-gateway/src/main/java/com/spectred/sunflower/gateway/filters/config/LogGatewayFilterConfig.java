package com.spectred.sunflower.gateway.filters.config;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class LogGatewayFilterConfig {
    private boolean consoleLog;

    private boolean cacheLog;
}
