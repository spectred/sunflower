package com.spectred.sunflower.gateway.predicates.attr;

import lombok.Data;

@Data
public class AgeConfig {
    private int min;
    private int max;
}
