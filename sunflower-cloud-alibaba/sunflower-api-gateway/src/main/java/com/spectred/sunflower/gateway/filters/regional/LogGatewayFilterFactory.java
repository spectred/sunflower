package com.spectred.sunflower.gateway.filters.regional;

import com.spectred.sunflower.gateway.filters.config.LogGatewayFilterConfig;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * 自定义局部过滤器
 */
@Component
public class LogGatewayFilterFactory extends AbstractGatewayFilterFactory<LogGatewayFilterConfig> {

    public LogGatewayFilterFactory() {
        super(LogGatewayFilterConfig.class);
    }

    @Override
    public List<String> shortcutFieldOrder() {
        return List.of("consoleLog", "cacheLog");
    }

    @Override
    public GatewayFilter apply(LogGatewayFilterConfig config) {
        return new GatewayFilter() {
            @Override
            public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
                if (config.isCacheLog()) {
                    System.out.println("cache log 开启");
                }
                if (config.isConsoleLog()) {
                    System.out.println("console log 开启");
                }
                return chain.filter(exchange);
            }
        };
    }
}
