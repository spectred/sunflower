package com.spectred.sunflower.gateway.predicates;

import com.spectred.sunflower.gateway.predicates.attr.AgeConfig;
import org.apache.commons.lang.StringUtils;
import org.springframework.cloud.gateway.handler.predicate.AbstractRoutePredicateFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import java.util.List;
import java.util.function.Predicate;

@Component
public class AgeRoutePredicateFactory extends AbstractRoutePredicateFactory<AgeConfig> {

    public AgeRoutePredicateFactory() {
        super(AgeConfig.class);
    }

    @Override
    public List<String> shortcutFieldOrder() {
        return List.of("min", "max");
    }

    @Override
    public Predicate<ServerWebExchange> apply(AgeConfig config) {
        Predicate<ServerWebExchange> age = serverWebExchange -> {
            String ageStr = serverWebExchange.getRequest().getQueryParams().getFirst("age");
            if (StringUtils.isNotBlank(ageStr)) {
                int age1 = Integer.parseInt(ageStr);
                return age1 > config.getMin() && age1 < config.getMax();
            }
            return true;
        };
        return age;
    }



}




