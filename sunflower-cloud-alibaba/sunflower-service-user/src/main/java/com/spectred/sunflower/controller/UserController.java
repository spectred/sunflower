package com.spectred.sunflower.controller;

import com.spectred.sunflower.annotation.RestPutMapping;
import com.spectred.sunflower.domain.User;
import com.spectred.sunflower.feign.OrderService;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Optional;


@RestController
public class UserController {

    @Resource
    private OrderService orderService;
    @Resource
    private RocketMQTemplate rocketMQTemplate;

    @GetMapping("/{id}")
    public String get(@PathVariable("id") Long id) {
        System.out.println("service-user:" + id);
        // 发送消息
        rocketMQTemplate.convertAndSend("demo-topic", id.toString());
        return orderService.get(id);
    }

    @RestPutMapping
    @ResponseStatus(value = HttpStatus.OK)
    public User put( @RequestBody User u) throws Exception {
        System.out.println(u.toString());
        return u;
    }
}
