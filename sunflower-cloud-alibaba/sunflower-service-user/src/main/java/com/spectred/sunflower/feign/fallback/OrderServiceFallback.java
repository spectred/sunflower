package com.spectred.sunflower.feign.fallback;

import com.spectred.sunflower.feign.OrderService;
import org.springframework.stereotype.Component;

@Component
public class OrderServiceFallback implements OrderService {
    @Override
    public String get(Long id) {
        return "Order Service error,default:-1";
    }
}
