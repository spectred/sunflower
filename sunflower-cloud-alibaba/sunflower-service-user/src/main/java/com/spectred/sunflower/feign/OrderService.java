package com.spectred.sunflower.feign;

import com.spectred.sunflower.feign.fallback.OrderServiceFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "sunflower-service-order", fallback = OrderServiceFallback.class)
public interface OrderService {

    @GetMapping("/{id}")
    String get(@PathVariable("id") Long id);
}
