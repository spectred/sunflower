package com.spectred.sunflower.api.payment.service.impl;

import com.spectred.sunflower.api.common.domain.Payment;
import com.spectred.sunflower.api.payment.dao.PaymentDao;
import com.spectred.sunflower.api.payment.service.IPaymentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Optional;

@Service
public class PaymentServiceImpl implements IPaymentService {

    @Resource
    private PaymentDao dao;

    @Override
    public Payment get(Long id) {
        Optional<Payment> byId = dao.findById(id);
        return byId.orElse(new Payment());
    }

    @Override
    public Payment save(Payment payment) {
        Payment save = dao.save(payment);
        return save;
    }
}
