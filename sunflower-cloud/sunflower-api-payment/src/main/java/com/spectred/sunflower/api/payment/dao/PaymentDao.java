package com.spectred.sunflower.api.payment.dao;

import com.spectred.sunflower.api.common.domain.Payment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentDao extends JpaRepository<Payment, Long> {
}
