package com.spectred.sunflower.api.payment.service;

import com.spectred.sunflower.api.common.domain.Payment;

public interface IPaymentService {

    Payment get(Long id);

    Payment save(Payment payment);
}
