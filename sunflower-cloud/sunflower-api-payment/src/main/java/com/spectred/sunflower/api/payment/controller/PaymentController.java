package com.spectred.sunflower.api.payment.controller;

import com.spectred.sunflower.api.common.domain.Payment;
import com.spectred.sunflower.api.common.dto.CommonResult;
import com.spectred.sunflower.api.payment.service.IPaymentService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/payment")
public class PaymentController {

    @Resource
    private IPaymentService paymentService;

    @Value("${server.port}")
    private String serverPort;

    @GetMapping(value = "/{id}")
    public CommonResult<Payment> get(@PathVariable("id") Long id) {
        Payment payment = paymentService.get(id);
        return new CommonResult<>(10, serverPort, payment);
    }

    @PostMapping
    public CommonResult<Payment> post(@RequestBody Payment payment) {
        Payment save = paymentService.save(payment);
        return new CommonResult<>(0, "success", save);
    }

}
