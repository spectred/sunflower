package com.spectred.sunflower.api.payment.controller;

import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/discovery/demo")
public class DiscoveryDemoController {
    @Resource
    private DiscoveryClient discoveryClient;

    @GetMapping("/services")
    public Object get() {
        List<String> services = discoveryClient.getServices();
        services.forEach(serviceId -> {
            List<ServiceInstance> instances = discoveryClient.getInstances(serviceId);
            instances.forEach(System.out::println);
        });
        return services;
    }
}
