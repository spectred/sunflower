package com.spectred.sunflower.api.order.controller;

import com.spectred.sunflower.api.common.domain.Payment;
import com.spectred.sunflower.api.common.dto.CommonResult;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.net.URI;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Resource
    private RestTemplate restTemplate;

    private static final String PAYMENT_URL = "http://API-PAYMENT-8002";

    @GetMapping("/{id}")
    public ResponseEntity<CommonResult> get(@PathVariable("id") Long id) {
        URI uri = URI.create(PAYMENT_URL + "/payment/" + id);
        return restTemplate.getForEntity(uri, CommonResult.class);
    }

    @PostMapping
    public ResponseEntity<CommonResult> post(@RequestBody Payment payment) {
        URI uri = URI.create(PAYMENT_URL + "/payment/");
        ResponseEntity<CommonResult> result = restTemplate.postForEntity(uri, payment, CommonResult.class);
        return result;
    }
}
