package com.spectred.sunflower.eureka.server.bak;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class EurekaServerBakApplication {
    public static void main(String[] args) {
        SpringApplication.run(EurekaServerBakApplication.class, args);
    }
}
